# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2011 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg

export_exlib_phases pkg_pretend src_prepare

SUMMARY="X.org driver for Synaptics touchpads"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        x11-server/xorg-server[>=1.12]
        x11-libs/libX11
        x11-libs/libevdev[>=0.4]
        (
            x11-libs/libXi[>=1.2]
            x11-libs/libXtst[>=1.1]
        ) [[ note = [ dependencies for synclient and syndaemon ] ]]
"

xf86-input-synaptics_pkg_pretend() {
    if [[ -f "${ROOT}"/etc/xorg.conf.d/50-synaptics.conf
          || -f "${ROOT}"/etc/X11/xorg.conf.d/50-synaptics.conf ]] ; then
        elog "Previous versions of ${PN} installed these files:"
        elog "    /etc/xorg.conf.d/50-synaptics.conf"
        elog "    /etc/X11/xorg.conf.d/50-synaptics.conf"
        elog
        elog "Due to config protect, they will not be removed automatically."
        elog "Please remove it manually."
    fi
}

xf86-input-synaptics_src_prepare() {
    # The default synaptics configuration in /usr/share/X11/xorg.conf.d/50-synaptics.conf
    # results in xinput trying to initialize the touchpad twice: first with evdev on
    # /dev/input/event* and then with synaptics on /dev/input/mouse*. On Linux
    # systems the first initialization succeeds and the second causes errors:
    #
    #     (EE) SynPS/2 Synaptics TouchPad no synaptics event device found
    #     (EE) Query no Synaptics: 6003C8
    #     (EE) SynPS/2 Synaptics TouchPad Unable to query/initialize Synaptics hardware.
    #     (EE) PreInit failed for input device "SynPS/2 Synaptics TouchPad"
    #
    # To prevent this, xinput's maintainer recommends that Linux packagers add
    # the following to the default synaptics configuration (overriding the
    # default evdev touchpad configuration):
    #
    #     MatchDevicePath "/dev/input/event*"
    #
    # synaptics upstream currently doesn't ship this change because it can
    # break initialization on non-Linux systems. For details see:
    #
    #     http://who-t.blogspot.com/2010/11/how-to-ignore-configuration-errors.html
    #
    edo sed \
        -e '\|MatchDevicePath "/dev/input/event\*"|s:^#::' \
        -i conf/70-synaptics.conf

    if ever is_scm; then
        autotools_src_prepare
    else
        default
    fi
}

